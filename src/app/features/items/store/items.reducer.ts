import { createReducer, on } from '@ngrx/store';
import { addItem, addItemSuccess, deleteItem, deleteItemSuccess, loadItemsSuccess } from './items.actions';
import { Item } from '../../../model/item';

// UPDATE
export const initialState: Item[] = [];

// UPDATE
export const itemReducer = createReducer(
  initialState,
  on(loadItemsSuccess, (state, action) => [...action.items]),
  on(addItemSuccess, (state, action) => [...state, action.item]),
  on(deleteItemSuccess, (state, action) => state.filter(item => item.id !== action.id)),
);
