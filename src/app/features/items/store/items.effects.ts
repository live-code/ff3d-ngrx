import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { mergeMap, catchError, map, switchMap, withLatestFrom, concatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {
  addItem,
  addItemFail, addItemSuccess,
  deleteItem,
  deleteItemFail, deleteItemSuccess,
  loadItems,
  loadItemsFail,
  loadItemsSuccess
} from './items.actions';
import { Item } from '../../../model/item';

@Injectable()
export class ItemsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    switchMap(() =>
      this.http.get<Item[]>('https://jsonplaceholder.typicode.com/users')
        .pipe(
          map(data => loadItemsSuccess({ items: data})),
          catchError(() => of(loadItemsFail())
          )
        )
    )
  ));
  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap((action) =>
      this.http.delete(`https://jsonplaceholder.typicode.com/users/${action.id}`)
        .pipe(
          map(() => deleteItemSuccess({ id: action.id})),
          catchError(() => of(deleteItemFail())
          )
        )
    )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    concatMap((action) =>
      this.http.post<Item>(`https://jsonplaceholder.typicode.com/users/`, action.item)
        .pipe(
          map((item) => addItemSuccess({ item })),
          catchError(() => of(addItemFail())
          )
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {}
}
