import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { NgForm } from '@angular/forms';

import { getItems } from './store/items.selectors';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/items.actions';
import { Item } from '../../model/item';
import { AppState } from '../../app.module';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'items-root',
  template: `
    <form #f="ngForm" (submit)="addItemHandler(f)" >
      <input type="text" [ngModel] name="name">
    </form>

    <li *ngFor="let item of (items$ | async)">
      {{item.name}}
      <button (click)="deleteHandler(item.id)">Delete</button>
    </li>
  `,
})
export class ItemsComponent {
  @ViewChild('f') form!: NgForm;
  items$: Observable<Item[]> = this.store.pipe(select(getItems));

  constructor(private store: Store<AppState>, private actions$: Actions) {
    this.store.dispatch(loadItems());

    this.actions$
      .pipe(ofType(addItemSuccess))
      .subscribe(() =>  this.form.reset())
  }

  deleteHandler(id: number): void {
   this.store.dispatch(deleteItem({ id }));
  }

  addItemHandler(form: NgForm): void {
    this.store.dispatch(addItem({ item: form.value }));

  }
}
