import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getTheme } from '../../core/theme/theme.selectors';
import { AppState } from '../../app.module';
import { setTheme } from '../../core/theme/theme.actions';

@Component({
  selector: 'ff-settings',
  template: `
    
    theme is {{theme$ | async}}
    <button (click)="setThemeHandler('light')">set light</button>
    <button (click)="setThemeHandler('dark')">set dark</button>
  `,
  styles: [
  ]
})
export class SettingsComponent {
  theme$: Observable<string> = this.store.select(getTheme)

  constructor(private store: Store<AppState>) { }

  setThemeHandler(value: 'dark' | 'light') {
    this.store.dispatch(setTheme({ theme: value}))
  }
}
