import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'items', loadChildren: () => import('./features/items/items.module').then(m => m.ItemsModule) },
  { path: 'signin', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
