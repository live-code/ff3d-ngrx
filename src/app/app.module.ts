import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { themeReducer, ThemeState } from './core/theme/theme.reducer';
import { NavbarComponent } from './core/navbar.component';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ItemsEffects } from './features/items/store/items.effects';
import { itemReducer } from './features/items/store/items.reducer';
import { Item } from './model/item';

export interface AppState {
  theme: ThemeState;
  items: Item[]
}

const reducers: ActionReducerMap<AppState> = {
  theme: themeReducer,
  items: itemReducer
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([ItemsEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
