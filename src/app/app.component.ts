import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'ff-root',
  template: `
    
    <ff-navbar></ff-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'ff3d-angular';

  constructor(private router: Router) {
    this.router.events
      .pipe(
        filter(ev => ev instanceof NavigationEnd),
        map((ev: any) => ev['url'])
      )
      .subscribe(ev => {
        console.log('analytics', ev)
      })
  }
}
