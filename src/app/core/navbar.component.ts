import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { fromEvent, interval, Observable, of } from 'rxjs';
import { getTheme } from './theme/theme.selectors';
import { Actions, ofType } from '@ngrx/effects';
import { setTheme } from './theme/theme.actions';
import { HttpClient } from '@angular/common/http';
import {
  catchError,
  concatMap,
  debounceTime,
  delay, distinct,
  distinctUntilChanged,
  exhaustMap, filter,
  map,
  mergeMap,
  switchMap,
  tap, withLatestFrom
} from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ff-navbar',
  template: `
    <div *ngIf="theme$ | async as theme" [ngClass]="{
      'bg-dark': theme === 'dark',
      'bg-light': theme === 'light'
    }">
      <button routerLink="items">items</button>
      <button routerLink="signin">login</button>
      <button routerLink="settings">settings</button>
    </div>
    
    <input type="text" [formControl]="input">
    
    <button #btn>button</button>
    {{theme$ | async}}
  `,
  styles: [`
    .bg-dark { background-color: #222}
    .bg-light { background-color: #ccc}
  `]
})
export class NavbarComponent {
  @ViewChild('btn') button!: ElementRef<HTMLButtonElement>
  theme$: Observable<string> = this.store.select(getTheme)

  input = new FormControl();


  constructor(
    private store: Store<AppState>, private actions$: Actions,
    private http: HttpClient
  ) {

    this.input.valueChanges
      .pipe(
        filter(text => text.length > 3),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(text => http.get<any[]>('https://jsonplaceholder.typicode.com/users?q=' + text)),
        map(users => users.length ? users[0].id : null),
        switchMap(
          id => http.get<any[]>('https://jsonplaceholder.typicode.com/todos?userId=' + id)
        ),
        withLatestFrom(this.store.select(getTheme))
      )
      .subscribe(([todos, theme]) => {
        console.log(todos, theme)
      })


    http.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .pipe(
        map(res => 123)
      )
      .subscribe(res => {
        console.log(res)
      })
    /*
    this.theme$.subscribe(console.log)

    // -----------------o|>
    this.actions$
      .pipe(
        ofType(setTheme)
      )
      .subscribe(console.log)*/
  }


  ngAfterViewInit() {
    fromEvent(this.button.nativeElement, 'click')
      .pipe(
        exhaustMap(
          () => this.http.get<any[]>('https://jsonplaceholder.typicode.com/users')
            .pipe(
              delay(1000)
            )
        )
      )
      .subscribe(res => {
        console.log(res)
      })
  }

}
