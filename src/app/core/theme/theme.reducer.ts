import { createReducer, on } from '@ngrx/store';
import { setTheme } from './theme.actions';

export interface ThemeState {
  name: 'dark' | 'light';
}

const initialState: ThemeState = {
  name: 'dark',
}

export const themeReducer = createReducer(
  initialState,
  on(setTheme, (state, action) => ({ name: action.theme}) )
)
