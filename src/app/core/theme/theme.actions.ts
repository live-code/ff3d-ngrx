import { createAction, props } from '@ngrx/store';

export const setTheme = createAction(
  '[theme] set ',
  props<{ theme: 'dark' | 'light' }>()
)
