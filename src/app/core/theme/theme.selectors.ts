import { AppState } from '../../app.module';

export const getTheme = (state: AppState) => state.theme.name
